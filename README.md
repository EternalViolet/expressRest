# ExpressRest

This is a simple ExpressJs Rest API.

## Requirements
- NodeJs

## Running
1. Install dependencies `npm install`
2. Run the server `npm start`