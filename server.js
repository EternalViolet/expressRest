'use strict';
let express = require('express'),
  app = express(),
  port = process.env.PORT || 8080;

app.listen(port);

// MIDDLEWARE

// Simple Logging
app.use(function(req, res, next) {
    console.log(`"${req.originalUrl}" accessed by ${req.ip}`);
    next();
});

// ROUTING
let helloRoutes = require('./api/routes/helloRoutes');
helloRoutes(app);

console.log('Express server started at: ' + port);