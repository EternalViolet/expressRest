exports.helloDefault = function(req, res) {
    res.json({ 'message': 'Hello there!' });
};

exports.helloName = function(req, res) {
    const name = req.params.name;
    res.json({ 'message': `Hello ${name}!` });
}
