module.exports = function(app) {
    let helloController = require('../controllers/helloController');

    app.route('/hello')
        .get(helloController.helloDefault);

    app.route('/hello/:name')
        .get(helloController.helloName);
};
